/* 
 * File:   ErrorCodes.h
 * Author: nkigen
 *
 * Created on 12 July 2013, 18:04
 */

/**
 * \file ErrorCodes.h
 * \brief Defines Error Codes for the program
 * \author nkigen
 * \author Elias
 *
 */

/**
 * \def CHECK_MESSAGE(var,pos)
  * \brief checks if a given bit at position pos is 1
 */

/**\def REGISTER_MESSAGE(var,pos)
 * \brief sets bit at position pos to 1
 */

/** \def TOOGLE_MESSAGE(var,pos)
  * \brief toggles bit at position pos
  */

/** \def ERROR_PROCESS_FRAME
  * \brief error in processing an image
 * This can be due to the wrong format communication error.
  */

/** \def ERROR_DETECT_CAMAERA
  * \brief error: cannot detect the camera module
  */

/** \def ERROR_SAVE_IMAGE
  * \brief error in saving the image
  */

 /** \var int errorInfo
  * \brief bitmap for the error codes
 */
#ifndef ERRORCODES_H
#define	ERRORCODES_H
#define CHECK_MESSAGE(var,pos)  !!(var & (1 << pos))
#define REGISTER_MESSAGE(var,pos) var |= 1 << pos
#define TOGGLE_MESSAGE(var,pos) var ^= 1 << pos
#define ERROR_PROCESS_FRAME 0
#define ERROR_DETECT_CAMERA 1
#define ERROR_SAVE_IMAGE 2
#define ERROR_CAMERA_RESPONSE 3
extern int errorInfo;

#endif	/* ERRORCODES_H */

