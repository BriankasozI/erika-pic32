/* 
 * File:   MessageInterface.h
 * Author: nkigen
 *
 * Created on 07 May 2013, 09:15
 */


/**
 * \file MessageInterface.h
 * \brief Defines functions that processes GUI operations
 * \author nelson kigen
 * \author Elias
 */

/**
 * \def MAIN_SCREEN
 * \brief Index of the main screen in the screen array
 */

/**
 * \def SD_SCREEN
 * \brief Index of the SD screen in the screen array
 */

/**
 * \def DELETE_IMG_SCREEN
 * \brief Index of the DELETE_IMG_SCREEN screen in the screen array
 */

/**
 * \def VIEW_IMAGE_SCREEN
 * \brief Index of the VIEW_IMAGE_SCREEN screen in the screen array
 */

/**
 * \def CAMERA_SCREEN
 * \brief Index of the CAMERA_SCREEN screen in the screen array
 */


#ifndef MESSAGEINTERFACE_H
#define	MESSAGEINTERFACE_H
 
#include "interface.h"
#include "GDD_Screens.h"
#include "ErrorCodes.h"
#include <ee.h>
#include "CMUCam2.h"
#define MAIN_SCREEN 0
#define SD_SCREEN 1
#define DELETE_IMG_SCREEN 2
#define VIEW_IMAGE_SCREEN 3
#define CAMERA_SCREEN 4

 

/**
 *  \var WORD currentGDDDemoScreenIndex
 *  \brief current screen being displayed
 *  */
extern WORD currentGDDDemoScreenIndex;
/** \var WORD messageBuffer 
 *  \brief BitMap for all widget messages
 */
extern WORD messageBuffer;
/** \var WORD errorBuffer
 *  \brief BitMap for all error messages
 */
extern WORD errorBuffer;
/** \var XCHAR xcharStringPtr
 *  \brief Temporary pointer required when adding items to the LISTBOX on SD_SCREEN screen
 */
extern XCHAR *xcharStringPtr;

/** \var XCHAR fileToDelete
 *  \brief Holds the name of the file to be deleted from the SD card
 */
extern XCHAR fileToDelete[12];

/** \var XCHAR fileToView
 *  \brief Holds the name of the file to be viewed from the SD card
 */
extern XCHAR fileToView[12];

//extern char *cameraStatusMsg;


extern int refreshDevices;
//BYTE detectMedia(); //detect SD card

extern char hardwareBuffer[2];

/**
 * \fn void updateWidgets()
 * \brief manages the update of all widgets on all the screens
 * Gets the current screen and calls the specific functions responsible for managing specific screens
 * \param void
 * \return void
 */
void updateWidgets();

/**
 * \fn update_sd_screen_widgets()
 * \brief Updates all widgets on the SD_SCREEN screen
 * manages the updates to the SD_SCREEN
 * \param void
 * \return void
 */
void update_sd_screen_widgets();
//void update_del_image_screen_widgets();

/**
 * \fn update_main_widgets()
 * \brief Updates all widgets on the MAIN_SCREEN screen
 * manages the updates to the MAIN_SCREEN
 * \param void
 * \return void
 */
void update_main_widgets();
/**
 * \fn update_image_view_screen()
 * \brief Updates all widgets on the VIEW_IMAGE_SCREEN screen
 * manages the updates to the VIEW_IMAGE_SCREEN
 * \param void
 * \return void
 */
void update_image_view_screen();
/**
 * \fn update_lsb3()
 * \brief Updates the contents pf the LISTBOX on SD_SCREEN screen
 * Updates contents of the LISTBOX with the list of filenames available in the SD card
 * \param void
 * \return void
 */
void update_lsb_3();

/**
 * \fn AddItemList(XCHAR *pText, LISTBOX *pLb)
 * \brief Add an entry to the LISTBOX
 * Adds an entry, pText to the LISTBOX pLb thereby increasing the number of entries by one
 * \param pText name of the entry to be added
 * \param pLb pointer to the LISTBOX structure
 * \return void
 */
void AddItemList(XCHAR *pText, LISTBOX *pLb);

void delete_image(); ///interface for deleting an image delete
void view_image(); ///interface for viewing an image

/**
 * \fn void update_camera_screen()
 * \brief Updates the CAMERA_SCREEN widgets
 * \param void
 * \return void
 */
void update_camera_screen();


/**
 * \fn void process_get_image()
 * \brief Interface to the Camera functionalities
 * Manages the capturing of images from the Camera and saving of the images to the SD card
 * \param void
 * \return void
 */
void process_get_image(int num);

// int cameraDetect();

#endif	/* MESSAGEINTERFACE_H */

