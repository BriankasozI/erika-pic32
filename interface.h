/* 
 * File:   interface.h
 * Author: nkigen
 *
 * Created on 24 April 2013, 00:15
 */

#ifndef INTERFACE_H
#define	INTERFACE_H


#include "SD.h"
#include "DataTypes.h"


#include <Graphics/Graphics.h>

#define LSTBOX_NUM_VIEW 5
#define HORIZONTAL_SLIDER_PAGE 2
#define VERTICAL_SLIDER_PAGE 3

 
#endif	/* INTERFACE_H */

