
#include "Main.h"
#include "MainDemo.h"
#include "DataTypes.h"
#include "SSD1926.h"

/**
 * \file SD.h
 * \brief Defines the interface to the SD card
 * \author nelson kigen
 *
 */

/**
 * \fn void sd_init()
 * \brief initializes the SD card
 * \param void
 * \return int 0=failure, 1=success
 */

/**
 * \fn void sd_open_image( char *name)
 * \brief Opens an image with the specified file name
 * \param name pointer to the file name
 * \return void
 */

/**
 * \fn char* sd_get_filelist(unsigned int *numItems)
 * \brief Gets the list of files in the SD card
 * \param numItems Pointer to a variable containing the number of the items in the file
 * \return void
 */

/**
 * \fn int32_t sd_delete_image( char *name)
 * \brief Deletes a file in the SD card with the specified name
 * \param name of the file to be deleted
 * \return 1 on success and -1 on error
 */

/**
 * \fn IMAGE_TYPE GetFileType(char *fileName)
 * \brief Gets the type of a given file
 * In this case the file is assumed to be an Image whose format is either JPG or RBG
 * which are supported by the image decoders.
 * \param fileName of the file/image whose format is desired
 * \return Image type whose format is specified in
 */

/**
 * \fn void PlayRGB(const char * fileName)
 * \brief Plays or displays a RGB movie/image
 * This function opens a RBG stream and displays it on the screen
 * \param fileName name of the file/image whose format is desired
 * \return Image type whose format is specified in DataTypes.h
 */

/**
 * \fn void getFileName(char *fname,IMAGE_TYPE type)
 * \brief Gets a pseudo-random filename
 * Generates a random filename to be used as the name of a file to be stored in the SD card
 * \param fname name of the file/image to be stored
 * \param type file/image type of the file to be stored
 * \return void
 */

/**
 * \fn void getrand_string(char *str, int8_t len)
 * \brief random string generator
 * Generates a random string of a specified length
 * \param str point to a buffer to store the string
 * \param len length of the string to be generated
 * \return void
 */

/**
 * \fn int16_t next_val()
 * \brief random number generator
 * Generates a random pseudorandom number. More lightweight than the rand() function since it
 * no floating point operations are used
 * \param str point to a buffer to store the string
 * \param len length of the string to be generated
 * \return void
 */



#define START_ADD   0ul
#define FRAME_SIZE (320ul * 240ul * 2ul)
#define SD_ABSENT 0
#define SD_PRESENT 1
/************************************************************
 * Externs: Function Prototypes
 ************************************************************/
extern BYTE FILEget_next_cluster(FSFILE *fo, DWORD n);
extern DWORD Cluster2Sector(DISK * disk, DWORD cluster);
extern BYTE SDSectorDMARead(DWORD sector_addr, DWORD dma_addr, UINT16 num_blk);
extern BYTE SDStatus;
extern BYTE rgbReg;
extern BYTE initSD;
extern FSFILE *rgbFile;
/************************************************************
 * Function Prototypes
 ************************************************************/
extern BYTE SDDetect(void);
void sd_init();
void sd_open_image(char *name);
int search_file(char *fname);
char* sd_get_filelist(unsigned int *numItems);
int32_t sd_delete_image(char *name); 
IMAGE_TYPE GetFileType(char *fileName);
void PlayRGB(const char * fileName);
void getFileName(char *fname, IMAGE_TYPE type);
void getrand_string(char *str, int8_t len);
char *setSDStatus();
int16_t next_val();
void putCMUImage(char *fname);